#include "FigureDrawer.h"

FigureDrawer::FigureDrawer(const int w, const int h, const char* title) {
	_w = w;
	_h = h;
	_gfx = new GraphicsAPI(title, w, h);
}

FigureDrawer::~FigureDrawer() {
	delete(_gfx);
	// Leak here, dont clean the vector
}

void FigureDrawer::blit() {
	_gfx->clear();
	for (Shape* s : _shapes) {
		// Waste to re-calculate each shape each frame..
		s->draw(_w, _h, (uint32_t*)_gfx->getPixels());
	}
	_gfx->blit();
}

void FigureDrawer::addShape(Shape* shape) {
	_shapes.push_back(shape);
}
