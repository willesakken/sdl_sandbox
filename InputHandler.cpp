#include "InputHandler.h"

bool InputHandler::getInput(int& action) {
    SDL_Event event;
    action = 0;
    
    while(SDL_PollEvent(&event)) {

        // Closing window
        if(event.type == SDL_QUIT) {
            return false;
        }

        if(event.type == SDL_KEYDOWN) {
            switch(event.key.keysym.sym) {
                case SDLK_ESCAPE:
                    return false;
                default:
                    // just return true instantly if there is some other activity
                    // other than what we're looking for so we dont have to 
                    // draw the new image if it is just mouse movement or something
					action = event.key.keysym.sym;
                    return true;
            }
        }

    }

    return true;
}