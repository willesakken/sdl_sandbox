#pragma once

#include "GraphicsAPI.h"
#include "Shape.h"
#include <SDL.h>
#include <vector>

// "Container" for which shapes to draw
class FigureDrawer {
private:
	int _w, _h;

	GraphicsAPI* _gfx;

	std::vector<Shape*> _shapes;

public:

	FigureDrawer(const int w, const int h, const char* title);

	~FigureDrawer();

	void blit();

	void addShape(Shape* shape);

};