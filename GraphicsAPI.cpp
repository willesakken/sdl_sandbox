#include "GraphicsAPI.h"

/**********
* PRIVATE *
***********/

Uint32 GraphicsAPI::getColor(int r, int g, int b, int a) {
	r = r & 0xFF;
	g = g & 0xFF;
	b = b & 0xFF;
	a = a & 0xFF;
	return (a << 24) + (r << 16) + (g << 8) + b;
}

/**********
 * PUBLIC *
 **********/

GraphicsAPI::GraphicsAPI(const char* title, int width, int height) {
	if(SDL_Init(SDL_INIT_VIDEO) != 0) {
		std::cout << "SDL init error: " << SDL_GetError() << std::endl;
		exit(0);
	}

	_w = width;
	_h = height;
	_color = 0xFFFFFFFF;

	_window = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, _w, _h, SDL_WINDOW_SHOWN);

	if (_window == nullptr) {
		std::cout << "Error creating the window, message: " << SDL_GetError() << std::endl;
		SDL_Quit();
		std::exit(0);
	}

	_renderer = SDL_CreateRenderer(_window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	if (_renderer == nullptr) {
		SDL_DestroyWindow(_window);
		std::cout << "Error creating the renderer, message: " << SDL_GetError() << std::endl;
		SDL_Quit();
		std::exit(0);
	}

	_texture = SDL_CreateTexture(_renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STREAMING, _w, _h);

	const int pixelCount = _w * _h;
	_pixelmap = (Uint32*)malloc(sizeof(Uint32)*pixelCount);
	for (int i = 0; i<pixelCount; i++) {
		_pixelmap[i] = 0xFFFFFFFF;
	}
}

GraphicsAPI::~GraphicsAPI() {
	free(_pixelmap);
	SDL_DestroyRenderer(_renderer);
	SDL_DestroyWindow(_window);
	SDL_Quit();
}

void GraphicsAPI::setColor(int r, int g, int b) {
	_color = ((0 << 24) + (r << 16) + (g << 8) + b);
}

void GraphicsAPI::blit() {
	// updates the texture from the pixels
	SDL_UpdateTexture(_texture, NULL, _pixelmap, _w*sizeof(Uint32));

	// shows the texture in the window
	SDL_RenderCopy(_renderer, _texture, NULL, NULL);
	SDL_RenderPresent(_renderer);
}

void GraphicsAPI::clear() {
	memset(_pixelmap, 0xFFFFFFFF, sizeof(int)*_w*_h);
	SDL_RenderClear(_renderer);
}

void GraphicsAPI::delay(int t) {
	SDL_Delay(t*1000);
}

int GraphicsAPI::getWidth() {
	return _w;
}

int GraphicsAPI::getHeight() {
	return _h;
}

void* GraphicsAPI::getPixels() {
	return (void*) _pixelmap;
}

void GraphicsAPI::putpixel(int x, int y) {
	_pixelmap[_w*y + x] = _color;
}

void GraphicsAPI::drawLine(int x1, int y1, int x2, int y2) {
	int dx = abs(x2 - x1);
	int dy = abs(y2 - y1);
	int stepX = 1;
	int stepY = 1;

	// draw initial pizel
	putpixel(x1, y1);

	// if the line is vertical/horizontal just draw it with a simple, much easier loop
	if (dx > 0 && dy == 0) {
		if (x2>x1) {
			for (int x=x1; x<x2; x++) putpixel(x, y1);
		} else {
			for (int x=x2; x<x1; x++) putpixel(x, y1);
		}

		return;
	}
	else if (dx == 0 && dy > 0) {
		if (y2>y1) {
			for (int y = y1; y<y2; y++) putpixel(x1, y);
		} else {
			for (int y = y2; y<y1; y++) putpixel(x1, y);
		}

		return;
	}

	// set step values
	if (x1 > x2)
		stepX = -1;
	if (y1 > y2)
		stepY = -1;

	// if the slope is shallow
	if (dx > dy) {
		int incE = 2 * dy;
		int incNE = 2 * (dy - dx);
		int d = 2 * dy - dx;

		while (x1 != x2) {
			if (d < 0) {
				d += incE;
			}
			else {
				d += incNE;
				y1 += stepY;
			}

			x1 += stepX;
			putpixel(x1, y1);
		}
	}
	// else it is steep
	else {
		int incE = 2 * dx;
		int incNE = 2 * (dx - dy);
		int d = 2 * dx - dy;

		while (y1 != y2) {
			if (d < 0) {
				d += incE;
			}
			else {
				d += incNE;
				x1 += stepX;
			}
			y1 += stepY;
			putpixel(x1, y1);
		}
	}
}

void GraphicsAPI::drawRect(int x, int y, int w, int h) {
	drawLine(x, y, x+w, y);
	drawLine(x, y+h, x+w, y+h);	
	drawLine(x, y+h, x, y);
	drawLine(x+w, y+h, x+w, y);
}

void GraphicsAPI::drawCircle(int x1, int y1, double radius) {
	int x = 0, y = radius, d = 1 - radius;

	// dont draw circles that are outside the window
	if (x1 < radius || y1 < radius) {
		return;
	}

	putpixel(x1, y1 + y);
	putpixel(x1, y1 + y);
	putpixel(x1, y1 - y);
	putpixel(x1, y1 - y);
	putpixel(x1 + y, y1);
	putpixel(x1 - y, y1);
	putpixel(x1 + y, y1);
	putpixel(x1 - y, y1);

	while (x < y) {
		if (d < 0) {
			d += 2 * x + 3;
		}
		else {
			d += 2 * (x - y) + 5;
			y--;
		}
		x++;

		putpixel(x1 + x, y1 + y);
		putpixel(x1 - x, y1 + y);
		putpixel(x1 + x, y1 - y);
		putpixel(x1 - x, y1 - y);
		putpixel(x1 + y, y1 + x);
		putpixel(x1 - y, y1 + x);
		putpixel(x1 + y, y1 - x);
		putpixel(x1 - y, y1 - x);
	}
}