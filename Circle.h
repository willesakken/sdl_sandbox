#pragma once

#include "Shape.h"

class Circle : virtual public Shape {
private:
	int _x, _y, _r;

public:

	Circle(const int x, const int y, const int r);
	Circle(const int x, const int y, const int r, const uint32_t color);

	void draw(const int w, const int h, uint32_t* pixelmap);

	// 1 = LEFT, 2 = DOWN, 3 = RIGHT, 4 = UP
	void move(const int dir);

	// 0 = SMALLER, 1 = BIGGER
	void resize(const int dir);
};

