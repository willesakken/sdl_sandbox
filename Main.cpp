#include <iostream>
#include <SDL.h>
#include "InputHandler.h"
#include "GraphicsAPI.h"
#include "FigureDrawer.h"
#include "Circle.h"

int main(int argc, char * argv[]) {
	const int w = 2160, h = 720, r = 300;
	InputHandler* input = new InputHandler();
	FigureDrawer* drawer = new FigureDrawer(w, h, "Slippery slope!");

	int x_centre = w - r - 100;
	int y_centre = h / 2;

	drawer->addShape(new Circle(x_centre, y_centre, r));

	int action = 0;
	Circle* shape = nullptr;
	while (input->getInput(action)) {
		switch (action) {
		case SDLK_n:
			shape = new Circle(x_centre, y_centre, r/2, 0x00FF8888);
			drawer->addShape(shape);
			break;
		case SDLK_m:
			shape = new Circle(x_centre, y_centre, r/2, 0x008888FF);
			drawer->addShape(shape);
			break;
		case SDLK_LEFT:
			shape->move(1);
			break;
		case SDLK_DOWN:
			shape->move(2);
			break;
		case SDLK_RIGHT:
			shape->move(3);
			break;
		case SDLK_UP:
			shape->move(4);
			break;
		case SDLK_PLUS:
			shape->resize(1);
			break;
		case SDLK_MINUS:
			shape->resize(0);
			break;
		}

		drawer->blit();
	}

	delete(input);
	delete(drawer);
	return 0;

}