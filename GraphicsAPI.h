#pragma once

#include <SDL.h>
#include <iostream>
#include <stdlib.h>
#include <cmath>


class GraphicsAPI {
	private:
		int _w, _h;
		Uint32 _backgroundColor;
		Uint32 _color;

		Uint32* _pixelmap;

		SDL_Window* _window;
		SDL_Renderer* _renderer;
		SDL_Texture* _texture;

		// packs rgb + alpha channel values into a Uint32 datatype
		Uint32 getColor(int r, int g, int b, int a);

	public:
		GraphicsAPI(const char* title, const int width, const int height);

		~GraphicsAPI();

		void blit();

		void clear();

		int getWidth();

		int getHeight();

		void* getPixels();

		// Delays program execution in seconds
		void delay(int t);

		// Set current drawing color
		void setColor(const int r, const int g, const int b);

		// Puts a pixel at x, y
		void putpixel(const int x, const int y);

		// Draws a line from x1,y1 to x2,y2
		void drawLine(const int x1, const int y1, const int x2, const int y2);

		// Draws a rectangle with w width and h heigh with top-left corner at x, y
		void drawRect(const int x, const int y, const int w, const int h);

		// Draws a circle with centre at x, y with radius r
		void drawCircle(const int x, const int y, const double r);
		
};