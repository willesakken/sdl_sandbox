#pragma once

#include <SDL.h>

class InputHandler { 
    public:
		// returns false if program should stop
		// keypress code returned in action variable
        bool getInput(int& action);

};