#pragma once

#include <tuple>
#include <vector>

class Shape {
protected:

	uint32_t _color = 0x00000000;

public:

	Shape() {};

	virtual void draw(const int w, const int h, uint32_t* pixelmap) = 0;

	// 1 = LEFT, 2 = DOWN, 3 = RIGHT, 4 = UP
	virtual void move(const int dir) = 0;

};

