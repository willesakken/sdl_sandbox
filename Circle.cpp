#include "Circle.h"

Circle::Circle(const int x, const int y, const int r) {
	_x = x;
	_y = y;
	_r = r;
}

Circle::Circle(const int x, const int y, const int r, const uint32_t color) {
	_x = x;
	_y = y;
	_r = r;
	_color = color;
}

void Circle::draw(const int w, const int h, uint32_t* pixelmap) {
	int x1 = 0, y1 = _r, d = 1 - _r;

	// dont draw circles that are outside the window
	if (_x < _r || _y < _r) {
		return;
	}

	pixelmap[_x + w*(_y + y1)] = _color;
	pixelmap[_x + w*(_y + y1)] = _color;
	pixelmap[_x + w*(_y - y1)] = _color;
	pixelmap[_x + w*(_y - y1)] = _color;

	while (x1 < y1) {
		if (d < 0) {
			d += 2 * x1 + 3;
		}
		else {
			d += 2 * (x1 - y1) + 5;
			y1--;
		}
		x1++;

		pixelmap[_x + x1 + w*(_y + y1)] = _color;
		pixelmap[_x - x1 + w*(_y + y1)] = _color;
		pixelmap[_x + x1 + w*(_y - y1)] = _color;
		pixelmap[_x - x1 + w*(_y - y1)] = _color;
		pixelmap[_x + y1 + w*(_y + x1)] = _color;
		pixelmap[_x - y1 + w*(_y + x1)] = _color;
		pixelmap[_x + y1 + w*(_y - x1)] = _color;
		pixelmap[_x - y1 + w*(_y - x1)] = _color;

	}
}

void Circle::move(const int dir) {
	switch (dir) {
	case 1: _x--; break;
	case 2: _y++; break;
	case 3: _x++; break;
	case 4: _y--; break;
	}
}

void Circle::resize(const int dir) {
	switch (dir) {
	case 0: _r--; break;
	case 1: _r++; break;
	}
}
